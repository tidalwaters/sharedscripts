#!/bin/bash 
#===============================================================================
#
#          FILE:  checknetwork
# 
#         USAGE:  ./checknetwork [help|always|never|down|limitN|restart]    (limit4 is default if nothing given)
# 
#   DESCRIPTION:  Test if the network is up and restart if necessary.  Run from crontab.
#					Example crontab entry (run script every 10 minutes):
#					*/10 * * * * /usr/local/bin/checknetwork
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  for best results install pkg mtr or mtr-tiny 
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Laurence McArthur (), lmcarthur@gmail.com
#       COMPANY: 
#       CREATED: 08/30/2011 09:29:43 AM EDT
#      REVISION: 06/06/2012 added mtr check to the pinginternet function
#							added logging to /var/log/network.log for pinginternet tests
#===============================================================================
## TODO:
	# - add a default $tests value so script will run without any initial configuration
	# - maybe the script can figure out if it is running on a network gateway machine or not
	# - add a check for log rotation if network.log is used, then add a rotation script to /etclogrotate.d/ if necessary.


set -o nounset                              # Treat unset variables as an error


## author notes:

# Original idea from :
# http://www.linuxquestions.org/questions/linux-networking-3/script-to-check-connection-and-restart-network-if-down-262281/
#   "also, to be on the safe side in my crontab i put a second line which restarts the network every hour regardless. So my crontab for root looks like this:
#   */2 * * * * /usr/local/bin/networkrestart
#   0 * * * *    service network restart > /dev/null"

# email notification and logging added to original
# add to check local upstream network connection first, then an internet host.
# if this script is run periodically as a cron job it is probably best not to run the while loops,
#   probably single pass if statements would be better suited
#   so that multiple instances will not be started by the next cron trigger.



## General comments:
## Normally test the internet host first, if that is OK then everything else must be good.
## Actually, that's probably all you need to do but this was an exercise in bash scripting.
##
##################################################


#-------------------------------------------------------------------------------
#  script variables, edit only with great caution  (leave this section first to initialize variables)
#-------------------------------------------------------------------------------
begintime=$(date +'%b %_d %Y %H:%M:%S')   #date/time formatted for email
gtwy=$(/sbin/route -n | /usr/bin/awk '/UG/ {print $2}')        		# prints gateway for this host
sent=no
mailmsg2=""      #initialize as empty
mailmsg3=""      #initialize as empty
mailmsg4=""      #initialize as empty
mailmsg5=""      #initialize as empty
netstatus=""     #initialize as empty
###################################################

#-------------------------------------------------------------------------------
#  user variables, edit as desired
#-------------------------------------------------------------------------------

# Suggestion if running this script on a gateway machine itself:
tests="pinginternet/exit ckgateway/cont pinggateway/exit"	# Here is where you specify which tests to run, 
															# the order they run in, and whether 
# Suggestion for an internal or workstation machine:        # to continue with the next test in line or exit if successful.
#tests="ckgateway/cont pinggateway/exit"						# The tests available are 
															# pinginternet, ckgateway, and pinggateway.
															# Each listed test must be followed by "/" and 
															# either "cont" or "exit", specifying to continue 
															# with the next test on success or finish this script.
															# The last test will always assume "exit",
															# as there is nothing to follow.

mailif=${1:-limit4}	#can be any one of: help, always, never, down, limit#, or restart.  default is limit4
	#always: mail everytime this script runs, even if all tests are successful 
			#(lots of email, sent each time cron triggers script)
	#down: email if this script finds network is down by any of the tests, even if restart is unsuccessful
	#limitN: will limit the number of emails sent on unsuccessful network restart attempts to the value of N.  example: limit6 will send a maximum of 6 emails 
	#restart: email only on a successful restart caused by this script
logif=down	#When to log to syslog, see above mailif for explanation.  can only be one of: always, never, down, or restart
logmsg="$0  network connectivity normal"	#syslog message on success
resultslog="/var/log/network.log"	#Where to log detailed test results, can be /dev/null for no logging
mailto=root@localhost	# leave empty ("") for no email at all
mailsubj="$HOSTNAME $0 NOTICE: Network test successful"									#initial email info
mailmsg1="The $HOSTNAME network was checked by the $0 script"	                        #to be used unless 
mailmsg2=" at $begintime.  The network was up and functioning normally at this time."	#changed by script below
#inet="google.com"	#internet address to ping for internet ping test, anything you can rely on being up
inet="8.8.8.8"	# Google DNS A server
varfile=/tmp/checknetwork		#could be "/var/lib/misc/checknetwork" but /tmp takes care of deleting the file on reboots so the limit,if any, on emails starts over on a roboot
##################################################

### define functions:

	#===  FUNCTION  ================================================================
	#          NAME:  mailit
	#   DESCRIPTION:  email results.  Useage: mailit $mailif
	#    PARAMETERS:  Any one of: always, never, down, limitN, restart. Should be taken from $mailif
	#       RETURNS:  
	#===============================================================================
mailit ()
{
	if [ "mailto" != "" ]; then	#check if an address is given, otherwise do not to email
		case $1 in # check mailif for a valid value
			always )
				echo -e "$mailmsg1$mailmsg2\n$mailmsg3\n$mailmsg4\n$mailmsg5" | /usr/bin/mail -s "$mailsubj" "$mailto"
				;;
			down )
				if [ "$netstatus" = down ] || [ "$netstatus" = up ] ; then
					echo -e "$mailmsg1$mailmsg2\n$mailmsg3\n$mailmsg4\n$mailmsg5" | /usr/bin/mail -s "$mailsubj" "$mailto"
				fi
				;;
			limit* )	#send limited number of emails on unsuccessful restart attempts
				if [ "$netstatus" = down ] ; then
					max=${mailif##limit}	#extract N from the mailif variable
					prev=$(wc -l < ${varfile})	#number of previous emails sent
					echo $(date) >> ${varfile}
						if [ ${prev} -lt ${max} ] ; then
							echo -e "$mailmsg1$mailmsg2\n$mailmsg3\n$mailmsg4\n$mailmsg5" | /usr/bin/mail -s "$mailsubj" "$mailto"
						fi
					elif [ "$netstatus" = up ] ; then
						echo -e "$mailmsg1$mailmsg2\n$mailmsg3\n$mailmsg4\n$mailmsg5" | /usr/bin/mail -s "$mailsubj" "$mailto"
				fi
				;;
			restart )
				if [ "$netstatus" = up ] ; then
					echo -e "$mailmsg1$mailmsg2\n$mailmsg3\n$mailmsg4\n$mailmsg5" | /usr/bin/mail -s "$mailsubj" "$mailto"
				fi
				;;
			never )
				#echo "do nothing"
				;;
			*)
				echo "Invalid or missing value for mailif, must be one of  always, never, down, limitN, or restart"
				;;
		esac    # --- end of case ---
	fi
}

	#===  FUNCTION  ================================================================
	#          NAME:  logit
	#   DESCRIPTION:  log results.  Useage: logit $logif
	#    PARAMETERS:  Any one of: always, never, down, restart. Should be taken from $logif
	#       RETURNS:  
	#===============================================================================
logit ()
{
if [ "$logmsg" != "" ]; then
		case $1 in # check mailif for valid value
			always )
				/usr/bin/logger -p daemon.alert -t network "$logmsg"
				;;
			down )
				if [ "$netstatus" = down ] || [ "$netstatus" = up ] ; then
					/usr/bin/logger -p daemon.alert -t network "$logmsg"
				fi
				;;
			restart )
				if [ "$netstatus" = up ] ; then
					/usr/bin/logger -p daemon.alert -t network "$logmsg"
				fi
				;;
			never )
				#echo "do nothing"
				;;
			*)
				echo "Invalid or missing value for logif, must be always, never, down, or restart"
				;;
		esac    # --- end of case ---
fi
}

	#===  FUNCTION  ================================================================
	#          NAME:  reportit
	#   DESCRIPTION:  send the mail, write log entry, and exit script
	#    PARAMETERS:  
	#       RETURNS:  
	#===============================================================================
reportit ()
{
	mailit $mailif
	logit $logif
	sent=yes
}	# ----------  end of function reportit  ----------

quit ()
{
	exit
}


ckgateway ()
{
#echo "now checking presence of gateway"  
	if [ -z "$gtwy" ]; then							# restart if no gateway is discovered
#        echo -e "The "$HOSTNAME" network is down by absent gateway.  Attempting to restart." 
        /etc/init.d/networking restart >/dev/null 2>&1
		mailsubj="$HOSTNAME $0 RESPONSE: Network has been restarted"
		mailmsg2=" and FAILED at $begintime."
		mailmsg3="A check for the existence of a network gateway showed that none was available."
		mailmsg4="An unsuccessful attempt was made to establish connectivity."
		logmsg="GATEWAY DOES NOT EXIST, an unsuccessful attempt was made to establish connectivity"
		netstatus=down

		# now check for gateway again to see if it came up
		gtwy=$(/sbin/route -n | /usr/bin/awk '/UG/ {print $2}')		# gateway for this host
		if [ -n "$gtwy" ] ; then                   		# on success do this
#			echo "SUCCESS by gateway existence:  the network is now up and connected normally!"
			donetime=$(date +'%b %_d %Y %H:%M:%S')   #time formatted for email		
			mailsubj="$HOSTNAME $0 RESPONSE: Network has been restarted"
			mailmsg4="Based on this check, the network was successfully restarted at $donetime."
			mailmsg5="The indicated gateway is now $gtwy. "
			logmsg="GATEWAY DID NOT EXIST, connectivity is now re-established"
			netstatus=up
		fi
		reportit
	fi
}	# ----------  end of function ckgateway  ----------
	
pinggateway () 			# ping the gateway
{
#echo "now checking ping to gateway" 
	(/bin/echo -e "\n\n" && /bin/date && /usr/bin/mtr -rn ${gtwy} && echo -e "\n") >> ${resultslog} 2>&1		#send mtr report to log before ping test
	pinggtwy=$(/bin/ping -c2 ${gtwy} >> ${resultslog} 2>&1 ; /bin/echo $?)
	if [ "$pinggtwy" != 0 ]; then       		# if gateway is up now, then ping to confirm
#		echo -e "The "$HOSTNAME" network is down by ping to gateway $gtwy .  Attempting to restart."
		/etc/init.d/networking restart >/dev/null 2>&1
		mailsubj="$HOSTNAME $0 RESPONSE: Network has been restarted"
		mailmsg2=" and FAILED at $begintime."
		mailmsg3="A ping to the network gateway ( $gtwy ) failed."
		mailmsg4="An unsuccessful attempt was made to establish connectivity."
		logmsg="GATEWAY ping test failed ($gtwy), an unsuccessful attempt was made to establish connectivity"
		netstatus=down

		# try ping again to see if restart succeeded
		pinggtwy=$(/bin/ping -c2 ${gtwy} 2>&1 ; /bin/echo $?) 			# ping the gateway
		if [ "$pinggtwy" = 0 ]; then       		# if gateway is up now, then ping to confirm
#			echo "SUCCESS by gateway ping:  the network is now up and connected normally!"
            donetime=$(date +'%b %_d %Y %H:%M:%S')   #time formatted for email      
			mailsubj="$HOSTNAME $0 RESPONSE: Network has been restarted"
			mailmsg4="Based on this check, the network was successfully restarted at $donetime."
			mailmsg5="A ping test to the indicated gateway ( $gtwy ) was successful."
			logmsg="GATEWAY ping test failed ($gtwy), connectivity is now re-established"
			netstatus=up
		fi
		reportit
	fi
}	# ----------  end of function pinggateway  ----------


pinginternet ()		# is network reaching the internet?

{
#echo "now checking ping to internet" 
	(/bin/echo -e "\n\n" && /bin/date && /usr/bin/mtr -rn ${inet} && echo -e "\n") >> ${resultslog} 2>&1		#send mtr report to log before ping test
	pinginet=$(/bin/ping -n -c2 ${inet} >> ${resultslog} 2>&1 ; /bin/echo $?)
#	pinginet=$(/bin/ping -c2 ${inet} >/dev/null 2>&1 ; /bin/echo $?)
	if [ "$pinginet" != 0 ]; then    # just to make sure all is well, ping an internet host
#		echo -e "The "$HOSTNAME" network is down by internet ping to $inet.  Attempting to restart."
		/etc/init.d/networking restart >/dev/null 2>&1
		mailsubj="$HOSTNAME $0 RESPONSE: Network has been restarted"
		mailmsg2=" and FAILED at $begintime."
		mailmsg3="A ping test to internet host ( $inet ) failed."
		mailmsg4="An unsuccessful attempt was made to establish connectivity."
		logmsg="INTERNET ping test failed ($inet), an unsuccessful attempt was made to establish connectivity"
		netstatus=down
		
		# ping the inet host again to see if network up now
		pinginet=$(/bin/ping -c2 ${inet} >/dev/null 2>&1 ; /bin/echo $?)		# is network reaching the internet?
		if [ "$pinginet" = 0 ] ; then		                  	# on success do this
#			echo "SUCCESS:  the network is now up and connected normally!"
            donetime=$(date +'%b %_d %Y %H:%M:%S')   #time formatted for email      
			mailsubj="$HOSTNAME $0 RESPONSE: Network has been restarted"
			mailmsg4="Based on this check, the network was successfully restarted at $donetime."
			mailmsg5="A ping test to internet host ( $inet ) was successful."
			logmsg="INTERNET ping test failed ($inet), connectivity is now re-established"
			netstatus=up
		fi
		reportit
	fi
}	# ----------  end of function pinginternet  ----------


#-------------------------------------------------------------------------------
#  Here's where the work gets done
#-------------------------------------------------------------------------------
if [ ${mailif} = help ]  ; then
	cat <<- END_TEXT
		usage:  $0 [help|always|never|down|limitN|restart]    (limit4 is default if nothing given)

		help	shows this usage text
		always	sends an email on every network restart attempt
		never	never sends email
		down	send email on all unsuccessful network restart attempts
		limitN	sends only N number of emails on unsuccessful restart attempts, but all successful attempts
		restart	sends email only on successful network restart attempts
	END_TEXT
	exit
fi
while [ -n "${tests}" ] ; do

	x=$(echo ${tests} | awk '{print $1}' | cut -d"/" -f1) 	#gets the next listed test to run
	y=$(echo ${tests} | awk '{print $1}' | cut -d"/" -f2) 	#gets the instruction (cont or exit) for the next listed test
	
	${x}	#run the test
	
	if [ $y = exit ] ; then
		tests=""
	fi
	
	tests=$(echo ${tests} | awk '{ $1 = ""; print }')	#chop the just run tests from $tests before continuing while loop

done

	#-------------------------------------------------------------------------------
	# if you get here then all the tests are completed 
	#-------------------------------------------------------------------------------

	if [ "$netstatus" = "" ]; then	# if $netstatus is still empty at this point then the network tests have all been successful so we will empty the var file if it had contents
#		echo "netstatus: $netstatus"
		> ${varfile}	#empty the var file
	fi
	if [ "$sent" = no ] ; then	#if reporting has not been yet, do it now
		reportit
	fi
	quit	
#echo "Script done, network is all up and running" 	
