#!/bin/bash - 
#===============================================================================
#
#          FILE:  UpdateCountryBlocklist
# 
#         USAGE:  ./UpdateCountryBlocklist [CountryCode1 [CountryCode2 [CountryCodeN]]]
#					Country code lists can also be placed in separate text file, or the Shorewall blrules file.
# 
#   DESCRIPTION:  Can be run as cron job or manually.  
#					Periodically download new IP lists for indicated countries and update the Shorewall firewall
#					blrules file to block those countries.
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  Shorewall firewall, wget
#          BUGS:  ---
#         NOTES:  Written to use on Debian firewall hosts, probably not portable. Not tested on anything else.
#					Many of the ip list sources are only updated every month or so
#					it is probably not necessary to run this any more often for update purposes.
#					I just place this file in /etc/cron.monthly/ unless I'm adding a new country to block.
#        AUTHOR: Laurence McArthur
#       COMPANY: 
#       CREATED: 09/12/2012 08:23:24 AM EDT
#      REVISION:  ---
#===============================================================================


#-------------------------------------------------------------------------------
#  IP lists by country are available from a number of internet sources.  Here are a few:
#		ipdeny.com, ipinfodb.com, countryipblocks.net, http://services.ce3c.be/ciprg/, etc.
#	I choose to use ipinfodb.com because I found it to be budget friendly (free) and reliable.
#
#	THIS SCRIPT IS WRITTEN TO USE ipinfo.com formatting, IT MAY NOT WORK ON OTHER SOURCES WITHOUT MODIFYING!
#
#	You can get a list of the 2 letter country codes from various places, here are a few:
#	http://ipinfodb.com/country.txt
#	http://www.hostip.info/bulk/countries.html
#	http://www.maxmind.com/app/iso3166
#		or you can look up an ip with whois and just use the country code listed for that country
#-------------------------------------------------------------------------------


set -o nounset                              # Treat unset variables as an error
set -o errexit                              # Exit immediately if a command exits with a non-zero status.

#-------------------------------------------------------------------------------
#  Set script variables.  Change as necessary.
#-------------------------------------------------------------------------------
clist=""                                  					# you can put a country code list here if you wish, or not
email="root@localhost"                           			# sysadmin to get emails, use "" for no mail
dliplist="http://ipinfodb.com/country_query.php?country=" 	# base url for country blocklists
dlcodelist="http://ipinfodb.com/country.txt"   				# url for country code list
blrules="/etc/shorewall/blrules"        		        	# path to blrules file
shorewall="/sbin/shorewall"
clistfile="path/to/file"             		# full path to txt file that has whitespace separated list of countries to block
usage="Usage: $0 [CountryCode1 [CountryCode2 [CountryCodeN]]]
Country codes can also be placed in a separate file, or the Shorewall blrules file.  See comments in $0"
updatelist="CODE   COUNTRY"                     # prints a column heading in a successful email message


#-------------------------------------------------------------------------------
# Country code lists can be in blrules file as a whitespace separated list on 
# one or more lines. Each such line must begin with "#Blocked Countries: "
#-------------------------------------------------------------------------------
# Retrieve any country code list that is in blrules file.
#-------------------------------------------------------------------------------
clist="$(awk -F: '/^#Blocked Countries:/ {print $2}' ${blrules}) ${clist}"

#-------------------------------------------------------------------------------
# Country code lists can be in separate file as a whitespace separated list on 
# one or more lines.
#-------------------------------------------------------------------------------
# Retrieve any country code list that is in a separate file located above by the $clistfile variable.
#-------------------------------------------------------------------------------
if [[ -s ${clistfile} ]] ; then 				# if list file exists and is not empty
	clist="$(cat "${clistfile}") ${clist}"       
fi

#-------------------------------------------------------------------------------
# If there is a country code list given as command line args append them to the list.
#-------------------------------------------------------------------------------
if [[ $# != 0 ]] ; then                         	
	clist="${clist} $@"
fi

#-------------------------------------------------------------------------------
# Make sure at least one country code is passed to script.
#-------------------------------------------------------------------------------
if [[ -z "${clist}" ]]  ; then
	echo -e "\nError: You must supply at least 1 country code as a command line argument, in blrules list, or other file list.\n"
	echo -e "${usage}"
	exit
fi

#-------------------------------------------------------------------------------
# Cleanup the list of country codes.
#-------------------------------------------------------------------------------
clist=$(sed 's/[[:blank:]]\+/\n/g' < <(echo "${clist}"))    # convert to single column list (replace whitespaces with newlines)
clist=$(sed 's/^$//g' < <(echo "${clist}"))    # delete any blank lines
clist=$(sort -fdu < <(echo "${clist}"))         # sort list and remove duplicates

#-------------------------------------------------------------------------------
# Download country code list and validate the country codes passed to this script, 
# also retrieve the full country name for later use.
#-------------------------------------------------------------------------------
codelist=$(wget -q -O - ${dlcodelist})
for cntry in ${clist} ; do
	if [[ (! ${#cntry} = 2) ]] ; then		#Test that each country code has 2 letters (alpha characters (or A1 or A2)).
		echo -e "\nError: \"${cntry}\"  is not a valid country code, all country codes are 2 characters each.\n"
		echo -e "${usage}"                              # print usage statement
		exit
	fi
	cntry=$(echo ${cntry} | tr a-z A-Z)         # Translate all country codes to upper case, standard for country code lists.
	cntryname=$(awk -v cntry=${cntry} '{if($1 == cntry) for(i=2;i<=NF;i++) print $i}' < <(echo "${codelist}"))
	if [[ ${cntryname} = "" ]] ; then	# If the country code does not match a listed country exit with error message.
		echo -e "\nError: \"${cntry}\"  is not a valid country code, check the country code list again.\n"
		echo -e "${usage}"                              # print usage statement
		exit
	fi
done

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Now that the country code list is complete, start editing.
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# But first make a backup of current blacklist file in case . . . 
#-------------------------------------------------------------------------------
/bin/cp -fp --backup=numbered ${blrules} ${blrules}.fwlw.bak

#-------------------------------------------------------------------------------
# Now delete previous automated entries in blrules file so we finish with only fresh ip's.
#-------------------------------------------------------------------------------
sed -i '/^[^#].*#.*auto-added/d' ${blrules} # Delete all previously automated country blocks.
											# Explanation: delete any line that does not start with "#", 
											# but has a "#" later in the line,
											# that is later followed by the word "auto-added".
sed -i '/^$/d' ${blrules}    # delete any remaining blank lines

#-------------------------------------------------------------------------------
# Loop through each country to download the ip list and edit blrules file to add the ip's.
#-------------------------------------------------------------------------------
for cntry in ${clist} ; do
	cntry=$(echo ${cntry} | tr a-z A-Z)         # Translate all country codes to upper case, standard for country code lists.
	cntryname=$(awk -v cntry=${cntry} '{if($1 == cntry) for(i=2;i<=NF;i++) printf "%s ", $i}' < <(echo "${codelist}"))
	updatelist="${updatelist} $(echo -e "\n${cntry}     ${cntryname}")" # To use in email messages.
	iplist=$(wget -q -O - ${dliplist}${cntry})   # Download the ip list for the current country.
	
	#-------------------------------------------------------------------------------
	#Clean up the ip list a bit.
	#-------------------------------------------------------------------------------
	iplist=$(sed 's/[[:blank:]]\+//g' < <(echo "${iplist}"))	# Remove whitespace from each line, mainly in case list
																# is provided in ip range format with spaces around the dash.
	iplist=$(sort -t. -k1,1n -k2,2n -k3,3n -k4,4n < <(echo "${iplist}")) # sort the list if necessary

	#-------------------------------------------------------------------------------
	#  Finally, add the newly obtained country ip blocks to the blrules file.
	#-------------------------------------------------------------------------------
	echo -e "\n" >> ${blrules}                  # Add an empty line to separate the countries.
	for ip in ${iplist}; do                     # Now add each ip in the new list to the Shorewall blrules file.
		printf "%-13s%-26s%-7s%s\n" "BLACKLIST" "net:${ip}" "all" "# ${cntryname} code ${cntry}  auto-added $(date +%D)" >> ${blrules}
	done
done

#-------------------------------------------------------------------------------
# Format an email message with the good news.
#-------------------------------------------------------------------------------
mailsubject="$HOSTNAME Successful Country Blocklist Update"
mailbody="Your host $HOSTNAME has successfully downloaded and activated current ip blocklists for the following countries:\n${updatelist}\n\n\nUsing the script  $0"

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#  Now that the blrules file is updated we need to restart shorewall to activate the new blacklisted country blocks.
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
set +e	# Do not allow a shorewall config error to stop the script here as we are handling it ourselves
		# so we can deliver an informative email about the error.
{ badconfig=$(${shorewall} -qq check 2>&1 1>&$out); } {out}>&1
	exit
set -e
if [ -n "${badconfig}" ] ; then
		/bin/cp -fp ${blrules}.fwlw.bak ${blrules}	#return file to original # I prefer to do this manually if necessary.
	mailsubject="$HOSTNAME $0 Shorewall restart failure"
	mailbody="$HOSTNAME shorewall attempted to restart but failed and returned the following:\n\n${badconfig}\n\nThis error should be corrected by manually editing the problem file.  Once corrected, you have several choices:\n\n1-  Examine the new ${blrules} file and compare to the backup that was made, if everything looks good you can run:\nshorewall restart    or,\n\n2-  Rerun $0."
else               # If you get here, everything looks good so restart shorewall, cleanup, and log, etc.
	${shorewall} restart> /dev/null 2>&1	#quietly restart since we have verified it is good
	/bin/rm -f ${blrules}.fwlw.bak*			#backups are no longer needed so remove them
	/usr/bin/logger -p daemon.alert -t Shorewall "Country blocklist has been updated (blrules)"		#log to syslog
fi

#-------------------------------------------------------------------------------
#  Email results to sysadmin.
#-------------------------------------------------------------------------------
echo -e "${mailbody}" | /usr/bin/mail -s "${mailsubject}" ${email}

exit
