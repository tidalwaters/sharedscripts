#!/bin/sh

# run this to to start tmux on a remote server something like:    ssh user@remote.org '/path/to/tmuxstart'
# and you will start with the specified windows, panes, sizes, apps running, etc OR restore previous session
# original inspirations from: http://superuser.com/questions/440015/restore-tmux-session-after-reboot

# In this script I use the following test syntax using expr for sh portability  expr $VAR : regex  , if using bash then you could do [[ ]] for regex match.  CHANGE: due to expr and regex engine differences in BSD systems it is not really portable (BSD only support basic regex so alternation with "|" not available).  Better to make a test using grep or egrep.

# The restore part of this script is dependent on using the tmux plugin tmux-continuum at https://github.com/tmux-plugins/tmux-continuum,
# 	Tip: To keep your restore directory somewhat under control I suggest periodically (maybe crontab) run: 
# 	find -H $HOME/.tmux/resurrect/ -mindepth 1 -maxdepth 1 -mtime +7 ! -samefile $HOME/.tmux/resurrect/last -type f -name tmux_resurrect_*.txt -delete
# 	This will delete all resurrect files older than 7 days but will always leave the file linked to last (usually the newest file).

# crontab:
# If you want the initial tmux windows and tabs in your favorite shell, then use a crontab line similar to this: SHELL=/bin/bash path/to/tmuxstart
# I often use this crontab line:  @reboot SHELL=/bin/bash /path/to/tmuxstart --noautorestore
# If you run this from crontab in an @reboot line you might prefer to start over with your preferred layout and programs because restore can't restore all programs.  If this is the case, you can start this script with the optional parameter of --noautorestore.  If you do this you can always restore manually to a previous saved session.
# OR, if you want to force the tmux server to start with automatic restore to previous saved session then use the optional parameter --forceautorestore.

tmuxstartlocal="${HOME}/.tmuxstart.local"   # The file containing local layout, programs, etc. when starting a new session.
## Some simple example lines that could be in your ${tmuxstartlocal} file
#		tmux -u new-session -x 59 -y 203 -s ${sessionname} -n "${firstwindowname}" -d
#  get the above -x and -y numbers from running in your terminal  $ stty size  or  $ echo -e "lines\ncols"|tput -S
#		tmux split-window -v -l 13
#		tmux select-pane -t 1
#		tmux send-keys -t ${sessionname} "irssi" ENTER 

restorescript="${HOME}/.tmux/plugins/tmux-resurrect/scripts/restore.sh"  # path to the restore script
firstwindowname="Main" 


usage=`cat <<EOF
Usage: $0  [--noautorestore|--forceautorestore|--help] [-t sessionname]

Started with no optional parameters, this script will:
	1) attach to an existing tmux session with the name defined in the optional sessionname parameter or the sessionname variable generated inside the script $0, OR
	2) ask the user to choose to restore a previously saved session (if one exists), OR
	3) start a new pre-configured session based on values in the script
		Note: localized pre-configured sessions can be written in the file ${tmuxstartlocal}

The option --noautorestore will skip the restore options even if a saved session does exist. 

The option --forceautorestore will restore without asking, but only if a saved session exists.

The optional sessionname argument will direct the script to attach, restore, or start a tmux session with the given sessionname.  If not given, the script will attempt to use the server hostname as the sessionname.
 
Started with the option --help will print this usage message.

 
EOF
`


# set -x 	# uncomment for testing the script

params="${@}"

# Check the command line for proper syntax
while [ $# -gt 0 ] ;do
	if [ $# -eq 1 ] &&  echo "${params}" | grep "^--noautorestore\|^--forceautorestore" >/dev/null  ;then
			break
		elif [ $# -eq 2 ] && expr "${params}" : "^-t .*" >/dev/null 2>&1 ;then
			sessionname="${2}"
			break
	 	elif [ $# -eq 3 ] && ! echo "${params}" | grep "^--help" >/dev/null && [ ${2} = -t ] ;then
			sessionname="${3}"
			break
 		else
			echo "${usage}"
			exit
	fi

done

## TODO:  if there is already more than one session running, allow choice of which to join
if [ -z "${sessionname}" ] ;then
## some of the following may not work in your environment, you can always just define sessionname="mysesionname"
sessionname="$(hostname)"
sessionname=${sessionname%.*}		#leaves only before the last dot (removes the domain name part)
sessionname=${sessionname##*.}  #if any dots remain, leaves only the part after the dots (leaves only the last significant part of hostname)
#sessionname=${sessionname:0:12}	#truncate to 8 characters  <-- this line is not portable and may need to be commented
fi


if ! tmux has-session -t "${sessionname}" >/dev/null 2>&1 ; then

	 	if ! expr "${params}" : "^--noautorestore" >/dev/null 2>&1  ; then
			if ! expr "${params}" : "^--forceautorestore" >/dev/null 2>&1 ; then
                # Unfortunately, it is not easy to get file modify time universally due to different stat commands so I do the best I can with the hack below.  
				# Right now I only have access to linux and a few BSD systems.  When I get info for other OSs I'll add it.  Added OSX (Darwin) from a friend's Mac.
                case $(uname) in
                    Linux) previous=$(stat -c %y ${HOME}/.tmux/resurrect/last | cut -d '.' -f1) ;;
                    *BSD) previous=$(stat -f %Sm ${HOME}/.tmux/resurrect/last) ;;
                    Darwin) previous=$(stat -f %Sm ${HOME}/.tmux/resurrect/last) ;; 
                    *) previous=" * not available in $(uname) * " ;;
                esac

				if [ -z "${previous}" ] ;then
					previous=" * Not Available * "
				fi
		        echo                                                                           
	    	    read -p "Confirm that you want to restore the ${previous} tmux environment (y) 
(Note:  If y then both will happen if the restorable session is different than \""${sessionname}"\")
OR start a fresh tmux session with name "${sessionname}" using your pre-configured settings (N)?"  REPLY
			else 
# no longer needed:
#				if [ ! -r ${HOME}/.tmux/resurrect/last ] ; then # if there is no saved session
#					echo "** You chose  $0 "${1}"  but there is no restore file available. Exiting... **"
#					exit
#				elif $(tmux has-session >/dev/null 2>&1) ; then  # is the tmux server running?
#					echo "The tmux server is running with available sessions.  An autorestore can only be done when starting the tmux server."
#					exit
#				fi
				REPLY=Y
			fi
	
					# autorestore:
		    	    if  expr "$REPLY" : "^[Yy]$" > /dev/null ; then
							if [ ! -r ${HOME}/.tmux/resurrect/last ] ; then # if there is no saved session
								echo
								echo "** You chose to autorestore but there is no restore file available. Exiting... **"
								exit
							elif $(tmux has-session >/dev/null 2>&1) ; then  # is the tmux server running?
								echo
								echo "** The tmux server is running with available sessions.  An autorestore can only be done when starting the tmux server. Exiting... **"
								exit
							fi
		 				rm -f ${HOME}/tmux_no_auto_restore			 
						tmux -u new-session -s ${sessionname} -n "${firstwindowname}" -d
						tmux run-shell ${restorescript}
			        fi
		 fi
########### end of autorestore section

		touch ${HOME}/tmux_no_auto_restore 	# this prevents the new session from trying to restore a previous session

		if [ -r ${tmuxstartlocal} ] && ! expr "$REPLY" : "^[Yy]$" > /dev/null ;then
			. ${tmuxstartlocal}
		elif ! expr "$REPLY" : "^[Yy]$" > /dev/null ;then
			# if you don't have a ${tmuxstartlocal} file then start with a generic session
			# this line is probably a good choice as first line in your ${tmuxstartlocal} file
			tmux -u new-session -s ${sessionname} -n "${firstwindowname}" -d
	 	fi

		rm -f ${HOME}/tmux_no_auto_restore			 
fi

if [ -t 0 ] ;then  # if shell is interactive (stdin connected to a terminal), attach to it
tmux -u attach -t ${sessionname} 	# attach to the session
fi
